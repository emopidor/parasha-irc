#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

import logging.config

import config


__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


# I need to configure logging before loading tornado modules,
# because they create their loggers during the load,
# but logging.config.dictConfig breaks all already created loggers.

if __name__ == "__main__":
    logging.config.dictConfig(config.logging)

    from server import run_server

    run_server()
