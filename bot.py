#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

import logging
import logging.config
import random
import string

from parasha.basic_user import BasicIrcUser
from parasha.basic_channel import ChannelRole
from parasha.client_connection import OnWireIrcUser
from parasha.channel_id import ChannelId
from parasha.nick import Nick


__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class IrcBot(BasicIrcUser):
    def __init__(self, ioloop, server, config):
        BasicIrcUser.__init__(self, server)
        self.ioloop = ioloop
        self.logger = logging.getLogger("IrcBot")
        self.config = config

    def setup(self):
        nick = Nick(u"бот")
        backup_nick = Nick(u''.join([random.choice(string.ascii_uppercase) for i in xrange(8)]))

        try:
            if self.server().has_user(nick):
                old_bot = self.server().user(nick)
                self.server().change_nick(old_bot, backup_nick)
            else:
                old_bot = None
        except:
            return

        try:
            self.initialize(nick)

            if old_bot:
                self.server().quit_user(old_bot, "Bot reload")
        except:
            self.server().quit_user(self, "fial")

            if old_bot:
                self.server().change_nick(old_bot, nick)


    def initialize(self, nick):
        self.server().add_user(self, u"fsb.ru")
        self.server().change_nick(self, nick)
        self.server().change_ident(self, u"ботик")
        self.server().change_real_name(self, u"Бендер Родригез")
        self.server().change_user_mode(self, self, {"o"}, set())

        self.server().join(self, ChannelId(u"#test"))
        self.server().channel(ChannelId(u"#test")).add_role(self, self, ChannelRole.owner)

        if self.server().channel(ChannelId(u"#test")).topic() is None:
            self.server().channel(ChannelId(u"#test")).set_topic(self, u"спасибо деду за победу")

    def __repr__(self):
        return "IrcBot"

    def is_admin(self, user):
        return (isinstance(user, OnWireIrcUser) and
                self.config.has_key("admin_host") and
                user.address()[0] == self.config["admin_host"])

    def privmsg(self, source, target, text):
        self.ioloop.spawn_callback(lambda: self.on_privmsg(source, target, text))

    def on_privmsg(self, source, target, text):
        self.logger.debug("Botik received privmsg from %s to %s: %s.",
                          repr(source.nick()),
                          repr(target),
                          repr(text))

        on_channel = isinstance(target, ChannelId)
        is_command = False
        command = ""

        if target is self:
            is_command = True
            command = text
        elif text.startswith(self.nick().original() + ":"):
            is_command = True
            command = text[len(self.nick().original()) + 1:].lstrip()

        if is_command:
            self.logger.debug("Trying a command from %s (on channel: %s): %s.",
                              repr(source.nick()),
                              repr(on_channel),
                              repr(command))

            if command == "go!":
                self.server().privmsg(self, ChannelId(u"#gogogo"), "gogogo!" * 500)
            elif command == "opme" and on_channel:
                self.server().channel(target).add_role(self, source, ChannelRole.op)
            elif command == "deopme" and on_channel:
                self.server().channel(target).revoke_role(self, source, ChannelRole.op)
            elif command == "hopme" and on_channel:
                self.server().channel(target).add_role(self, source, ChannelRole.hop)
            elif command == "dehopme" and on_channel:
                self.server().channel(target).revoke_role(self, source, ChannelRole.hop)
            elif command == "voiceme" and on_channel:
                self.server().channel(target).add_role(self, source, ChannelRole.voiced)
            elif command == "devoiceme" and on_channel:
                self.server().channel(target).revoke_role(self, source, ChannelRole.voiced)
            elif command.startswith("setident"):
                self.server().change_ident(source, command[len("setident"):].strip())
            elif command.startswith("sethost"):
                self.server().change_host(source, command[len("sethost"):].strip())
            elif command.startswith("setname"):
                self.server().change_real_name(source, command[len("setname"):].lstrip())
            elif command == "reload" and self.is_admin(source):
                self.server().reload_world()

    def join(self, user, channel):
        self.ioloop.spawn_callback(lambda: self.on_join(user, channel))

    def on_join(self, user, channel):
        if self.config.get("silent", False):
            return

        self.server().privmsg(self, channel, str(user.nick()) + ": " + "oh hi")
        self.server().notice(self, user, "hello ( ͡͡ ° ͜ʖ ͡ °)")

    def do_kick(self, who, channel, reason):
        self.ioloop.spawn_callback(lambda: self.on_do_kick(channel, who))

    def on_do_kick(self, channel, who):
        self.server().join(self, channel)
        self.server().channel(channel).add_role(self, self, ChannelRole.owner)
        self.server().kick(self, channel, who, "azaza")
