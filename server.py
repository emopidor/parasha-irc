#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

import logging

import ipaddress

from tornado.tcpserver import TCPServer
from tornado.ioloop import IOLoop

from parasha.client_connection import OnWireIrcUser
import parasha.server

import config
import bot


__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class IrcServer(parasha.server.IrcServer):
    def __init__(self, ioloop):
        parasha.server.IrcServer.__init__(self, u"name", logger = logging.getLogger("Server"))
        self.ioloop = ioloop
        self.bot = None

    def set_name(self, name):
        self.full_name = lambda: name

    def set_server_info(self, info):
        self.server_info = lambda: info

    def set_motd(self, motd):
        self.motd = lambda: motd

    def reload_bot(self):
        reload(bot)

        new_bot = bot.IrcBot(self.ioloop, self, config.bot)

        try:
            new_bot.setup()
            self.bot = new_bot
        except:
            pass

    def reload_config(self):
        reload(config)

        self.set_name(config.server_name)
        self.set_server_info(config.server_info)
        self.set_motd(config.motd)

        config.logging["incremental"] = True
        logging.config.dictConfig(config.logging)

    def reload_world(self):
        self.reload_config()
        self.reload_bot()


class TcpFronend(TCPServer):
    def __init__(self, ioloop):
        TCPServer.__init__(self, ioloop)

        self.ioloop = ioloop
        self.server = IrcServer(ioloop)
        self.server.reload_world()

    def handle_stream(self, stream, address):
        logging.debug("New connection accepted from address %s.", repr(address))
        new_client = OnWireIrcUser(self.ioloop, self.server, stream, address)
        host = config.users_host or ipaddress.ip_address(address[0].decode("ascii")).exploded
        self.server.add_user(new_client, host)
        new_client.run()


def run_server():
    ioloop = IOLoop.instance()
    server = TcpFronend(ioloop)
    server.listen(config.port)
    ioloop.start()
