[PEP-8](https://www.python.org/dev/peps/pep-0008/) is used with some changes and restrictions:

* Source files encoding is utf-8.

* The maximum length of lines is 100 characters.

* Indentation is 4 spaces per level.

* Methods in classes are _always_ surrounded by a single blank line.

    Yes:

        class ClassLois(object):
            def dummy_method1(self):
                pass

            def dummy_method2(self):
                pass

            def dummy_method3(self):
                pass

    No:

        class ClassLois(object):
            def dummy_method1(self):
                pass
            def dummy_method2(self):
                pass
            def dummy_method3(self):
                pass

* `if` statement and its body should _never_ be on the same line.

    Yes:

        if True:
            do_things()

    No:

        if True: do_things()

* Write all sequences (function arguments, lists, long conjustions/disjunctions) in one line or
  place each element on its own line.

    Yes:

        f(arg1, arg2, arg3)

        f(arg1,
          arg2,
          arg3)

        if (A and
            B and
            C):

        if ((A or B) and
            C):

    No:

        f(arg1, arg2, arg3,
          arg4, arg5)

        [item1, item2, item3,
         item4, item5]

        if (A and B and C and
            D and E):

* In multi-line function signatures/calls and lists all arguments/elements are aligned with the first one.
  For the first argument/element, you have two options:

    1. Placing it on the same line with the opening bracket:

            def f(arg1,
                  arg2)

        In this case the closing bracket is placed on the same line with the last argument.

    2. Placing it on a separate line:

            def very_long_function_name(
                arg1,
                arg2
            ):

            list = [
                arg1,
                arg2
            ]

        The closing bracket is placed on a separate line and aligned with the start of the whole construction.

    You should prefer the first variant and use the second one only to avoid sticking the sequence items
    to the right side of the screen.

* Single quote (`'`) is not used for string quoting.

* Spaces should surround all binary operators.

    Yes:

        x * x + y * y

        def f(a, b = None):

    No:

        x*x + y*y

        def f(a, b=None):

* Words in lower-case names should always be separated with underscore character (`_`).

    Yes:

        def is_digit():

    No:

        def isdigit():

* All conditions should be explicit.

    Yes:

        if len(sequence) > 0:

        if integer_number != 0:

        if whatever is not None:

    No:

        if sequence:

        if integer_number:

        if whatever:

    It doesn't apply to boolean variables used as conditions, but if the boolean variable doesn't have
    semantics of condition, and you're just looking up its value, you should compare explicitly.

* Use `assert`s to check arguments and preconditions in functions.

* Only new-style classes. Inherit all your classes from `object`.

__Note:__ If some of the existing code contradicts these rules, it doesn't mean that new code may do the same =^_^=
