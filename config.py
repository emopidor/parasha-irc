#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


port = 6667

server_name = u"parasha.irc"

server_info = u"Public Parasha test server"

motd = [
u"Welcome to the Parashaland!",
u"",
u"Work hard and become the King of Chooxans!"
]

users_host = None

bot = {
    "silent": True,
    "admin_host": "127.0.0.1"
}

logging = {
    "version": 1,
    "formatters": {
        "fmt": {
            "format": "[%(asctime)s] [%(levelname)8s]: %(name)s: %(message)s",
            "datefmt": "%d/%m/%Y %H:%M:%S"
        }
    },
    "handlers": {
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "DEBUG",
            "formatter": "fmt",
            "filename": "parasha.log",
            "maxBytes": 10 * 1024 * 1024,
            "backupCount": 10
        },
        "stdout": {
            "class": "logging.StreamHandler",
            "formatter": "fmt",
            "level": "DEBUG",
            "stream": "ext://sys.stdout"
        }
    },
    "root": {
        "level": "DEBUG",
        "handlers": ["file", "stdout"]
    }
}
