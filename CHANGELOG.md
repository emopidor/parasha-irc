# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased][unreleased]
### Added
- Behavior of OnWireIrcUser can be customized via subclassing or irc messages handlers.
- Support '\n' as IRC messages separator. Because mIRC uses it.
- Ability to detach a user from an irc server without terminating it (new method: BasicIrcServer.release_user).
### Fixed
- Several minor bugs like uncaught exceptions or mistyped identifiers.

## 0.1.2 - 2015-05-02
### Added
- Setup script.
- License.
### Fixed
- Fixed command WHO crashing the server when called with channel name and "o" flag.
- Now when user changes own flags, server doesn't replies if nothing actually changed.

## 0.1.1 - 2015-05-01
### Fixed
- IRC commands are case-insensitive now.
- A bug when the server returns incorrect user's role on a channel.

## 0.1.0 - 2015-05-01
### Added
- The embeddable Parasha IRC server.
- It handles all mandatory IRC messages from RFC 2812 except sections 3.4 and 3.5.
- Server and channels logic is customizable via subclassing.
- Implementation of the client protocol is not customizable.
  You may hack something from client_connection.py, but it may break in any minor release.
- Example server and bot are provided in the root directory of the project.
