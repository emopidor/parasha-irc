#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

from parasha import config

setup(
    name = "Parasha",

    version = config.short_version,

    description = "An embeddable IRC server",

    long_description = "A library implementing a simple IRC server. Serving IRC clients has never been easier. It's AMAZING.",

    url = "https://bitbucket.org/emopidor/parasha-irc",

    author = "Kirisaki Chitoge",

    author_email = "unity@hardcore.mytishi",

    license = "LGPLv3+",

    classifiers = [
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Topic :: Communications :: Chat :: Internet Relay Chat",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7"
    ],

    keywords = "IRC server 0chan",

    zip_safe=False,

    packages = ["parasha"],

    install_requires = ["tornado", "enum34"],

    extras_require = {
        "dev": ["ipaddress"]
    }
)
