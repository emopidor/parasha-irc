#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

import logging
import re
import random
import string
import time

import enum

import tornado.iostream

import parasha.errors
from parasha.channel_id import ChannelId
from parasha.nick import Nick
from parasha.basic_connection import BasicIrcConnection, IrcConnectionState
from parasha.basic_channel import ChannelRole
from parasha.basic_user import BasicIrcUser


__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


def modes_to_string(to_add = None, to_remove = None):
    modes = ""

    if to_add and len(to_add) > 0:
        modes += "+" + "".join(to_add)

    if to_remove and len(to_remove) > 0:
        modes += "-" + "".join(to_remove)

    if len(modes) == 0:
        return "+"
    else:
        return modes


class IrcClientState(enum.Enum):
    awaiting_nick = 1
    awaiting_user = 2
    awaiting_pong = 3
    chatting = 4
    terminating = 5


class IrcClientConnection(BasicIrcConnection):
    def __init__(self, user, ioloop, stream, address):
        BasicIrcConnection.__init__(self, stream, user.logger())

        self.__user = user
        self.__server = user.server()
        self.__ioloop = ioloop
        self.__address = address
        self.__client_state = IrcClientState.awaiting_nick

        self.__last_ping = None

        self.set_handler("QUIT", self._process_quit_message)
        self.set_handler("PING", self._process_ping_message)
        self.set_handler("PONG", self._process_pong_message)
        self.set_handler("NICK", self._process_nick_message)
        self.set_handler("USER", self._process_user_message)
        self.set_handler("SERVICE", self._process_service_message)
        self.set_handler("JOIN", self._process_join_message)
        self.set_handler("PART", self._process_part_message)
        self.set_handler("PRIVMSG", self._process_privmsg_message)
        self.set_handler("NOTICE", self._process_notice_message)
        self.set_handler("MODE", self._process_mode_message)
        self.set_handler("TOPIC", self._process_topic_message)
        self.set_handler("WHOIS", self._process_whois_message)
        self.set_handler("WHO", self._process_who_message)
        self.set_handler("WHOWAS", self._process_whowas_message)
        self.set_handler("OPER", self._process_oper_message)
        self.set_handler("SQUIT", self._process_squit_message)
        self.set_handler("NAMES", self._process_names_message)
        self.set_handler("LIST", self._process_list_message)
        self.set_handler("INVITE", self._process_invite_message)
        self.set_handler("KICK", self._process_kick_message)
        self.set_handler("KILL", self._process_kill_message)
        self.set_handler("AWAY", self._process_away_message)
        self.set_handler("MOTD", self._process_motd_message)

        self.ioloop().call_later(30, self.__on_initialization_timeout)

    def __on_initialization_timeout(self):
        if self.client_state() in {IrcClientState.awaiting_nick, IrcClientState.awaiting_user}:
            self.quit("Initialization timeout.")

    def user(self):
        return self.__user

    def server(self):
        return self.__server

    def ioloop(self):
        return self.__ioloop

    def address(self):
        return self.__address

    def client_state(self):
        return self.__client_state

    def quit(self, reason):
        self.logger().debug("Calling quit: %s.", repr(reason))

        self.server().quit_user(self.user(), reason)

    def drop(self, reason):
        self.logger().debug("Calling drop: %s.", repr(reason))

        self.__client_state = IrcClientState.terminating
        self.server().quit_user(self.user(), reason)

    def terminate(self, reason):
        if self.state() != IrcConnectionState.disconnected:
            self.logger().debug("Closing the connection: %s.", repr(reason))

            if self.client_state() != IrcClientState.terminating:
                self.send_message(self.server().full_name(), "ERROR", [reason] if reason else [])

            self.close()

    def send_message(self, prefix, command, args, *method_args, **method_kvargs):
        def check_type(arg):
            assert (arg is None or
                    isinstance(arg, str) or
                    isinstance(arg, unicode) or
                    isinstance(arg, ChannelId) or
                    isinstance(arg, Nick))

        check_type(prefix)
        check_type(command)

        for arg in args:
            check_type(arg)

        BasicIrcConnection.send_message(self, prefix, command, args, *method_args, **method_kvargs)

    def _on_close(self):
        self.logger().debug("The connection is closed.")

        if self.state() != IrcConnectionState.disconnected:
            BasicIrcConnection._on_close(self)
            self.drop("Client disconnected.")
        else:
            BasicIrcConnection._on_close(self)

    def _do_ping(self):
        if self.state() == IrcConnectionState.disconnected:
            return

        if self.__last_ping is not None:
            self.quit("Ping timeout.")
        else:
            self.__last_ping = ''.join([random.choice(string.ascii_uppercase) for i in xrange(8)])
            self.send_message(None, "PING", [self.__last_ping])
            self.ioloop().call_later(180, self._do_ping)

    # Functions processing the client's input.
    def on_unknown_message(self, message):
        self.err_unknowncommand(message.command)

    def process_message(self, message):
        if message.command == "QUIT":
            self._process_quit_message(message)
            return
        elif message.command == "PING":
            self._process_ping_message(message)
            return

        if self.client_state() in {IrcClientState.awaiting_nick, IrcClientState.awaiting_user}:
            self._process_authentication_stage(message)
        elif self.client_state() == IrcClientState.awaiting_pong:
            self._process_awaiting_pong_state(message)
        elif self.client_state() == IrcClientState.chatting:
            BasicIrcConnection.process_message(self, message)

    def _check_arguments_number(self, message, required):
        if len(message.args) < required:
            self.err_needmoreparams(message.command)
            return False
        else:
            return True

    def _process_authentication_stage(self, message):
        if message.command == "NICK":
            if self._process_nick_message(message):
                self.logger().debug("Nickname accepted. " +
                                    "Switching to state IrcClientState.awaiting_user.")

                self.__client_state = IrcClientState.awaiting_user
            else:
                return
        elif message.command == "USER":
            if not self._process_user_message(message):
                return
        else:
            self.err_notregistred(message.command)

        if self.client_state() == IrcClientState.awaiting_user and self.user().ident() is not None:
            self.logger().debug("Nickname and user info received. " +
                                "Switching to state IrcClientState.awaiting_pong.")

            self.__client_state = IrcClientState.awaiting_pong
            self._do_ping()

    def _process_awaiting_pong_state(self, message):
        if message.command == "NICK":
            self._process_nick_message(message)
        elif message.command == "USER":
            self._process_user_message(message)
        elif message.command == "PONG":
            if self._process_pong_message(message):
                self.logger().info("The client authorized: %s.", repr(self.user().full_name()))

                self.__client_state = IrcClientState.chatting
                self.send_welcome_sequence()
        else:
            self.err_notregistred(message.command)

    def _process_nick_message(self, message):
        if len(message.args) == 0:
            self.err_nonicknamegiven()
            return False
        else:
            try:
                self.server().change_nick(self.user(), Nick(message.args[0]))
                return True
            except parasha.errors.NickInUseError as e:
                self.err_nicknameinuse(e.nick)
            except parasha.errors.InvalidNickError as e:
                self.err_erroneusnickname(e.nick)

            return False

    def _process_user_message(self, message):
        if not self._check_arguments_number(message, 4):
            return False
        elif self.user().ident() is not None:
            self.err_alreadyregistred()
            return False
        else:
            ident = message.args[0].split("@")[0][:self.server().max_ident_length()]
            self.server().change_ident(self.user(), ident)
            self.server().change_real_name(self.user(), message.args[3])

            if message.args[1] == "4":
                self.server().change_user_mode(self.user(), self.user(), {"w"}, set())
            elif message.args[1] == "8":
                self.server().change_user_mode(self.user(), self.user(), {"i"}, set())
            elif message.args[1] == "12":
                self.server().change_user_mode(self.user(), self.user(), {"w", "i"}, set())

            return True

    def _process_service_message(self, message):
        if self._check_arguments_number(message, 6):
            self.err_alreadyregistred()

    def _process_ping_message(self, message):
        self.send_message(None, "PONG", message.args)

    def _process_pong_message(self, message):
        if self._check_arguments_number(message, 1):
            if message.args[0] == self.__last_ping:
                self.__last_ping = None
                return True

        return False

    def _process_quit_message(self, message):
        self.quit(None if len(message.args) == 0 else message.args[0])

    def _process_join_message(self, message):
        if self._check_arguments_number(message, 1):
            for channel_name in message.args[0].split(","):
                try:
                    self.server().join(self.user(), ChannelId(channel_name))
                except parasha.errors.NoSuchChannelError as e:
                    self.err_nosuchchannel(e.channel)
                except parasha.errors.InvalidChannelError as e:
                    self.err_nosuchchannel(e.channel)
                except parasha.errors.AlreadyOnChannelError:
                    pass

    def _process_part_message(self, message):
        if self._check_arguments_number(message, 1):
            reason = message.args[1] if len(message.args) > 1 else None
            for channel_name in message.args[0].split(","):
                try:
                    self.server().part(self.user(), ChannelId(channel_name), reason)
                except parasha.errors.NoSuchChannelError as e:
                    self.err_nosuchchannel(e.channel)
                except parasha.errors.NotOnChannelError as e:
                    self.err_notonchannel(e.channel)

    def _process_privmsg_message(self, message):
        if len(message.args) == 0:
            self.err_norecipient("PRIVMSG")
        elif len(message.args) == 1:
            self.err_notexttosend()
        else:
            try:
                self.server().privmsg(self.user(), message.args[0], message.args[1])
            except parasha.errors.NoSuchNickError as e:
                self.err_nosuchnick(e.nick)
            except parasha.errors.CantSendToChannelError as e:
                self.err_cannotsendtochan(e.channel)

    def _process_notice_message(self, message):
        if len(message.args) == 0:
            self.err_norecipient("NOTICE")
        elif len(message.args) == 1:
            self.err_notexttosend()
        else:
            try:
                self.server().notice(self.user(), message.args[0], message.args[1])
            except parasha.errors.NoSuchNickError as e:
                self.err_nosuchnick(e.nick)
            except parasha.errors.CantSendToChannelError as e:
                self.err_cannotsendtochan(e.channel)

    def _process_mode_message(self, message):
        if self._check_arguments_number(message, 1):
            target = message.args[0]

            if len(message.args) == 1:
                if self.server().has_nick(Nick(target)):
                    target_user = self.server().user(Nick(target))
                    if target_user is self.user():
                        self.rpl_umodeis(target_user.modes())
                    else:
                        self.err_usersdontmatch()
                elif self.server().has_channel(ChannelId(target)):
                    self.rpl_channelmodeis(target, self.server().channel(ChannelId(target)).modes())
                else:
                    self.err_nosuchnick(target)
            else:
                # Changing modes is not supported. Just reply with any appropriate error.
                if self.server().has_channel(ChannelId(target)):
                    self.err_nochanmodes(target)
                else:
                    self.err_umodeunknownflag()

    def _process_topic_message(self, message):
        if self._check_arguments_number(message, 2):
            channel_id = ChannelId(message.args[0])

            try:
                channel = self.server().channel(channel_id)
            except:
                self.err_nosuchchannel(channel_id)

            channel.set_topic(self.user(), message.args[1], int(time.time()))

    def __reply_whois(self, nick):
        if not self.server().has_nick(nick):
            self.err_nosuchnick(nick)
        else:
            user = self.server().user(nick)

            self.rpl_whoisuser(self.server(), user)
            self.rpl_whoisserver(self.server(), user)

            def channel_with_user_status(channel, user):
                prefix = self.server().channel(channel).member_data(user).role().nick_prefix()
                return prefix + channel.name()

            channel_strings = [
                channel_with_user_status(channel, user)
                for channel
                in self.server().user_data(user).channels()
            ]

            self.rpl_whoischannels(
                self.server(),
                user,
                "".join(channel + " " for channel in channel_strings)
            )

            self.rpl_endofwhois(self.server(), user)

    def _process_whois_message(self, message):
        if len(message.args) == 0:
            self.err_nonicknamegiven()
            return
        elif len(message.args) == 1:
            nicks_to_process = message.args[0]
        elif message.args[0] != self.server().full_name():
            self.err_nosuchserver(message.args[0])
            return
        else:
            nicks_to_process = message.args[1]

        for nick in nicks_to_process.split(","):
            self.__reply_whois(Nick(nick))

    def __get_who(self, query, ops_only):
        if query in (None, "!"):
            if ops_only:
                return [(None, user) for user in self.server().users() if user.is_ircop()]
            else:
                return [(None, user) for user in self.server().users()]
        elif self.server().has_channel(ChannelId(query)):
            channel_id = ChannelId(query)
            channel = self.server().channel(channel_id)
            members = list(channel.members())

            if ops_only:
                return [(channel_id, user)
                        for user
                        in members
                        if channel.member_data(user).role() == ChannelRole.op]
            else:
                return [(channel_id, user) for user in members]
        elif self.server().has_nick(Nick(query)):
            result = self.server().user(Nick(query))
            if ops_only:
                return [(None, result)] if result.is_ircop() else []
            else:
                return [(None, result)]
        else:
            return []

    def _process_who_message(self, message):
        query = None
        ops_only = False

        if len(message.args) > 0:
            query = message.args[0]

        if len(message.args) > 1 and message.args[1] == "o":
            ops_only = True

        for channel, user in self.__get_who(query, ops_only):
            self.rpl_whoreply(channel, user)

        self.rpl_endofwho(query)

    def _process_whowas_message(self, message):
        if len(message.args) == 0:
            self.err_nonicknamegiven()
        else:
            self.err_wasnosuchnick(message.args[0])

    def _process_oper_message(self, message):
        if self._check_arguments_number(message, 2):
            self.err_nooperhost()

    def _process_squit_message(self, message):
        if self._check_arguments_number(message, 2):
            self.err_noprivileges()

    def _process_names_message(self, message):
        if len(message.args) > 0:
            channels = [ChannelId(channel_name) for channel_name in message.args[0].split(",")]
        else:
            channels = None

        if len(message.args) > 1 and message.args[1] != self.server().full_name():
            self.err_nosuchserver(message.args[1])
        else:
            self.send_names(channels)

    def _process_list_message(self, message):
        if len(message.args) > 0:
            channels = [ChannelId(channel_name) for channel_name in message.args[0].split(",")]
        else:
            channels = self.server().channels()

        if len(message.args) > 1 and message.args[1] != self.server().full_name():
            self.err_nosuchserver(message.args[1])
        else:
            for channel in channels:
                self.rpl_list(channel)

            self.rpl_listend()

    def _process_invite_message(self, message):
        if self._check_arguments_number(message, 2):
            self.err_chanoprivsneeded(ChannelId(message.args[1]))

    def _process_kick_message(self, message):
        if self._check_arguments_number(message, 2):
            reason = message.args[2] if len(message.args) > 2 else None

            for channel in message.args[0].split(","):
                for nick in message.args[1].split(","):
                    channel_id = ChannelId(channel)
                    try:
                        self.server().kick(self.user(), channel_id, Nick(nick), reason)
                    except parasha.errors.NoSuchChannelError as e:
                        self.err_nosuchchannel(e.channel)
                    except parasha.errors.NoSuchNickError as e:
                        self.err_usernotinchannel(e.nick, channel_id)
                    except parasha.errors.NotOnChannelError as e:
                        self.err_usernotinchannel(e.user.nick(), e.channel)

    def _process_kill_message(self, message):
        if self._check_arguments_number(message, 2):
            self.err_noprivileges()

    def _process_away_message(self, message):
        if len(message.args) == 0:
            self.server().change_user_mode(self.user(), self.user(), set(), {"a"})
            self.rpl_unaway()
        else:
            self.server().change_user_mode(self.user(), self.user(), {"a"}, set())
            self.rpl_nowaway()

    def _process_motd_message(self, message):
        if len(message.args) > 0 and message.args[0] != self.server().full_name():
            self.err_nomotd()
        else:
            self.send_motd()

    # Functions to send various stuff.
    def send_motd(self):
        motd = self.server().motd()
        if motd:
            self.rpl_motdstart()

            for line in motd:
                self.rpl_motd(line)

            self.rpl_endofmotd()
        else:
            self.err_nomotd()

    def send_welcome_sequence(self):
        self.rpl_welcome()
        self.rpl_yourhost()
        self.rpl_created()
        self.rpl_myinfo()
        self.rpl_isupport()
        self.send_motd()

    def send_names(self, channels):
        channels_users = dict()

        for channel in channels or self.server().channels():
            try:
                channels_users[channel] = self.server().channel(channel).members()
            except:
                # Just ignore inexistent channels.
                pass

        if channels is not None:
            other_users = None
        else:
            users_on_channels = set(sum(channels_users.values(), []))
            other_users = set(self.server().users()) - users_on_channels

        for channel, members in channels_users.items():
            def nick_with_status(user, channel):
                prefix = self.server().channel(channel).member_data(user).role().nick_prefix()
                return prefix + user.nick().original()

            member_strings = [nick_with_status(user, channel) for user in members]

            self.rpl_namereply(channel, member_strings)
            self.rpl_endofnames(channel)

        if other_users is not None and len(other_users) > 0:
            self.rpl_namereply("*", [user.nick().original() for user in other_users])
            self.rpl_endofnames("*")

    def rpl_motdstart(self):
        self.send_message(
            self.server().full_name(),
            "375",
            [self.user().nick(), "- %s Message of the day - " % self.server().full_name()]
        )

    def rpl_motd(self, text):
        self.send_message(self.server().full_name(), "372", [self.user().nick(), "- " + text])

    def rpl_endofmotd(self):
        self.send_message(self.server().full_name(), "376", [self.user().nick(), "End of MOTD command"])

    def rpl_notopic(self, channel):
        self.send_message(self.server().full_name(), "331", [self.user().nick(), channel, "No topic is set"])

    def rpl_topic(self, channel):
        self.send_message(self.server().full_name(), "332", [self.user().nick(), channel, self.server().channel(channel).topic()])

    def rpl_topicwhotime(self, channel):
        channel_data = self.server().channel(channel)

        self.send_message(self.server().full_name(),
                          "333",
                          [self.user().nick(), channel, channel_data.topic_setter(), str(channel_data.topic_timestamp())])

    def rpl_namereply(self, channel, members):
        self.send_message(self.server().full_name(), "353", [self.user().nick(), "=", channel, " ".join(members)])

    def rpl_endofnames(self, channel):
        self.send_message(self.server().full_name(), "366", [self.user().nick(), channel, "End of NAMES list"])

    def rpl_welcome(self):
        welcome_message = "Welcome to the Internet Relay Network %s" % self.user().full_name()
        self.send_message(self.server().full_name(), "001", [self.user().nick(), welcome_message])

    def rpl_yourhost(self):
        message = "Your host is %s, running version %s" % (self.server().full_name(), self.server().version())
        self.send_message(self.server().full_name(), "002", [self.user().nick(), message])

    def rpl_created(self):
        message = "This server was created %s" % self.server().created()
        self.send_message(self.server().full_name(), "003", [self.user().nick(), message])

    def rpl_myinfo(self):
        args = [self.server().full_name(),
                self.server().version(),
                "".join(self.server().supported_umodes()),
                "".join(self.server().supported_cmodes())]

        self.send_message(self.server().full_name(), "004", [self.user().nick()] + args)

    def rpl_isupport(self):
        args = [name + "=" + value for name, value in self.server().isupport().items()]

        self.send_message(self.server().full_name(), "005", [self.user().nick()] + args + ["are supported by this server"])

    def rpl_umodeis(self, umodes):
        modes_string = modes_to_string(to_add = umodes)
        self.send_message(self.server().full_name(), "221", [self.user().nick(), modes_string])

    def rpl_channelmodeis(self, channel, cmodes):
        modes_string = modes_to_string(to_add = cmodes)
        self.send_message(self.server().full_name(), "324", [self.user().nick(), channel, modes_string, ""])

    def rpl_whoisuser(self, who, user):
        self.send_message(who.full_name(),
                          "311",
                          [self.user().nick(), user.nick(), user.ident(), user.host(), "*", user.real_name()])

    def rpl_whoisserver(self, who, user):
        self.send_message(who.full_name(),
                          "312",
                          [self.user().nick(), user.nick(), user.server().full_name(), user.server().server_info()])

    def rpl_whoisidle(self, who, user, seconds):
        self.send_message(who.full_name(), "317", [self.user().nick(), user.nick(), str(seconds), "seconds idle"])

    def rpl_whoischannels(self, who, user, channels):
        self.send_message(who.full_name(), "319", [self.user().nick(), user.nick(), channels])

    def rpl_endofwhois(self, who, user):
        self.send_message(who.full_name(), "318", [self.user().nick(), user.nick(), "End of WHOIS list"])

    def rpl_whoreply(self, channel, user):
        flags = "G" if user.is_away() else "H"

        if user.is_ircop():
            flags += "*"

        if channel is not None:
            if self.server().channel(channel).member_data(user).role() == ChannelRole.op:
                flags += "@"
            elif self.server().channel(channel).member_data(user).role() == ChannelRole.voiced:
                flags += "+"

        args = [channel or "*",
                user.ident(),
                user.host(),
                user.server().full_name(),
                user.nick(),
                flags,
                str(self.server().hopcount(user.server())) + " " + user.real_name()]

        self.send_message(self.server().full_name(), "352", [self.user().nick()] + args)

    def rpl_endofwho(self, query):
        self.send_message(self.server().full_name(), "315", [self.user().nick(), query, "End of WHO list"])

    def rpl_list(self, channel):
        channel_data = self.server().channel(channel)
        args = [channel, str(len(channel_data.members())), channel_data.topic()]
        self.send_message(self.server().full_name(), "322", [self.user().nick()] + args)

    def rpl_listend(self):
        self.send_message(self.server().full_name(), "323", [self.user().nick(), "End of LIST"])

    def rpl_nowaway(self):
        self.send_message(self.server().full_name(), "306", [self.user().nick(), "You have been marked as being away"])

    def rpl_unaway(self):
        self.send_message(self.server().full_name(), "305", [self.user().nick(), "You are no longer marked as being away"])

    def err_nomotd(self):
        self.send_message(self.server().full_name(), "422", [self.user().nick(), "MOTD File is missing"])

    def err_nonicknamegiven(self):
        self.send_message(self.server().full_name(), "431", [self.user().nick(), "No nickname given"])

    def err_erroneusnickname(self, nick):
        self.send_message(self.server().full_name(), "432", [self.user().nick(), nick, "Erroneous nickname"])

    def err_nicknameinuse(self, nick):
        self.send_message(self.server().full_name(), "433", [self.user().nick(), nick, "Nickname is already in use"])

    def err_unavailresource(self, resource):
        self.send_message(self.server().full_name(), "437", [self.user().nick(), resource, "Nick/channel is temporarily unavailable"])

    def err_restricted(self):
        self.send_message(self.server().full_name(), "484", [self.user().nick(), "Your connection is restricted!"])

    def err_needmoreparams(self, command):
        self.send_message(self.server().full_name(), "461", [self.user().nick(), command, "Not enough parameters"])

    def err_alreadyregistred(self):
        self.send_message(self.server().full_name(), "462", [self.user().nick(), "Unauthorized command (already registered)"])

    def err_usernotinchannel(self, nick, channel):
        self.send_message(self.server().full_name(), "441", [self.user().nick(), nick, channel, "They aren't on that channel"])

    def err_notonchannel(self, channel):
        self.send_message(self.server().full_name(), "442", [self.user().nick(), channel, "You're not on that channel"])

    def err_nosuchchannel(self, channel):
        self.send_message(self.server().full_name(), "403", [self.user().nick(), channel, "No such channel"])

    def err_nosuchnick(self, nick):
        self.send_message(self.server().full_name(), "401", [self.user().nick(), nick, "No such nick/channel"])

    def err_cannotsendtochan(self, channel):
        self.send_message(self.server().full_name(), "404", [self.user().nick(), channel, "Cannot send to channel"])

    def err_norecipient(self, command):
        self.send_message(self.server().full_name(), "411", [self.user().nick(), "No recipient given (%s)" % command])

    def err_notexttosend(self):
        self.send_message(self.server().full_name(), "412", [self.user().nick(), "No text to send"])

    def err_usersdontmatch(self):
        self.send_message(self.server().full_name(), "502", [self.user().nick(), "Cannot change mode for other users"])

    def err_uknownmode(self, mode, channel):
        self.send_message(self.server().full_name(),
                          "472",
                          [self.user().nick(), mode, "is unknown mode char to me for " + channel.name()])

    def err_nochanmodes(self, channel):
        self.send_message(self.server().full_name(),
                          "477",
                          [self.user().nick(), channel, "Channel doesn't support modes"])

    def err_umodeunknownflag(self):
        self.send_message(self.server().full_name(), "501", [self.user().nick(), "Unknown MODE flag"])

    def err_nosuchserver(self, server):
        self.send_message(self.server().full_name(), "402", [self.user().nick(), server, "No such server"])

    def err_nooperhost(self):
        self.send_message(self.server().full_name(), "491", [self.user().nick(), "No O-lines for your host"])

    def err_noprivileges(self):
        self.send_message(self.server().full_name(), "481", [self.user().nick(), "Permission Denied- You're not an IRC operator"])

    def err_notregistred(self, command):
        self.send_message(self.server().full_name(), "451", [command, "You have not registered"])

    def err_chanoprivsneeded(self, channel):
        self.send_message(self.server().full_name(), "482", [self.user().nick(), channel, "You're not channel operator"])

    def err_wasnosuchnick(self, nick):
        self.send_message(self.server().full_name(), "406", [self.user().nick(), nick, "There was no such nickname"])

    def err_unknowncommand(self, command):
        self.send_message(self.server().full_name(), "421", [self.user().nick(), command, "Unknown command"])


class OnWireIrcUser(BasicIrcUser):
    def __init__(self, ioloop, server, stream, address, logger = None):
        BasicIrcUser.__init__(self, server)
        self.__logger = logger or server.logger().getChild("client-" + repr(address))
        self.__connection = self._create_connection(ioloop, stream, address)

    def _create_connection(self, ioloop, stream, address):
        return IrcClientConnection(self, ioloop, stream, address)

    def logger(self):
        return self.__logger

    def connection(self):
        return self.__connection

    def address(self):
        return self.connection().address()

    def __repr__(self):
        return "OnWireIrcUser(%s)" % repr(self.address())

    def run(self):
        self.connection().run()

    # IrcUser interface implementation.
    def terminate(self, reason = None):
        self.connection().terminate(reason)

    def privmsg(self, source, target, text):
        self.connection().send_message(source.full_name(), "PRIVMSG", [target.short_name(), text])

    def notice(self, source, target, text):
        self.connection().send_message(source.full_name(), "NOTICE", [target.short_name(), text])

    def user_mode_change(self, who, user, add, remove):
        self.connection().send_message(who.full_name(),
                                       "MODE",
                                       [user.nick(), modes_to_string(add, remove)],
                                       without_colon = True)

    def role_change(self, who, channel, action, flag, user):
        self.connection().send_message(who.full_name(),
                                       "MODE",
                                       [channel, action + flag, user.nick()],
                                       without_colon = True)

    def set_nick(self, nick):
        if self.connection().client_state() != IrcClientState.awaiting_nick:
            self.nick_change(self, nick)

    def nick_change(self, who, new_nick):
        self.connection().send_message(who.full_name(), "NICK", [new_nick])

    def do_join(self, channel):
        self.join(self, channel)

        if self.server().channel(channel).topic() is None:
            self.connection().rpl_notopic(channel)
        else:
            self.connection().rpl_topic(channel)
            self.connection().rpl_topicwhotime(channel)

        self.connection().send_names([channel])

    def join(self, user, channel):
        self.connection().send_message(user.full_name(), "JOIN", [channel])

    def do_part(self, channel, reason):
        self.part(self, channel, reason)

    def part(self, user, channel, reason):
        self.connection().send_message(user.full_name(),
                                       "PART",
                                       [channel] + ([reason] if reason is not None else []))

    def quit(self, user, reason):
        self.connection().send_message(user.full_name(),
                                       "QUIT",
                                       [reason] if reason is not None else [])

    def topic(self, who, channel, text):
        self.connection().send_message(who.full_name(), "TOPIC", [channel, text])

    def do_kick(self, who, channel, reason):
        self.kick(who, channel, self, reason)

    def kick(self, who, channel, user, reason):
        self.connection().send_message(
            who.full_name(),
            "KICK",
            [channel, user.nick()] + ([reason] if reason is not None else [])
        )
