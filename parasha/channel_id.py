#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class ChannelId(object):
    def __init__(self, name):
        assert isinstance(name, unicode)

        self._name = name.lower()

    def name(self):
        return self._name

    # Target interface implementation.
    def short_name(self):
        return self.name()

    def __eq__(self, other):
        assert isinstance(other, ChannelId)

        return self.name() == other.name()

    def __ne__(self, other):
        return not (self == other)

    def __hash__(self):
        return hash(self.name())

    def __str__(self):
        return self.name().encode("utf-8")

    def __repr__(self):
        return "ChannelId(%s)" % repr(self.name())

def normalize_to_channel(what):
    if isinstance(what, unicode):
        return ChannelId(what)
    elif isinstance(what, ChannelId):
        return what
    else:
        return None
