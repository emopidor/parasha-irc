#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

import logging
import time

import enum

import parasha.errors


__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class ChannelRoleProperties(object):
    def __init__(self, flags, name_prefix, priority):
        assert len(flags) <= 1
        assert len(name_prefix) <= 1

        self.flags = flags
        self.prefix = name_prefix
        self.priority = priority


class ChannelRole(enum.Enum):
    owner = ChannelRoleProperties({"q"}, "~", 0)
    admin = ChannelRoleProperties({"a"}, "&", 1)
    op = ChannelRoleProperties({"o"}, "@", 2)
    hop = ChannelRoleProperties({"h"}, "%", 3)
    voiced = ChannelRoleProperties({"v"}, "+", 4)
    user = ChannelRoleProperties(set(), "", 5)

    def nick_prefix(self):
        return self.value.prefix

    def flags(self):
        return self.value.flags

    def priority(self):
        return self.value.priority

    def __repr__(self):
        return "ChannelRole." + self.name


def channel_role_prefixes():
    return set(role.nick_prefix() for role in ChannelRole)


class ChannelMemberData(object):
    def __init__(self):
        self.modes = set()

    def role(self):
        result = ChannelRole.user

        for role in ChannelRole:
            if role.flags() & self.modes == role.flags() and role.priority() < result.priority():
                result = role

        return result


class ChannelMember(object):
    def __init__(self, data):
        self._data = data

    def modes(self):
        return self._data.modes

    def role(self):
        return self._data.role()


class BasicIrcChannel(object):
    def __init__(self,
                 server,
                 id,
                 topic = None,
                 topic_setter = u"",
                 topic_timestamp = 0,
                 logger = None):
        assert topic is None or isinstance(topic, unicode)
        assert isinstance(topic_setter, unicode)

        self._server = server
        self._id = id
        self._topic = topic
        self._topic_setter = topic_setter
        self._topic_timestamp = topic_timestamp
        self._logger = logger or server.logger().getChild("channel-" + id.name())
        self._users = dict()
        self._modes = set()

    def __repr__(self):
        return "BasicIrcChannel(%s/%s)" % (repr(self.server()), repr(self.id()))

    def logger(self):
        return self._logger

    def server(self):
        return self._server

    def name(self):
        return self._id.name()

    def id(self):
        return self._id

    def topic(self):
        return self._topic

    def topic_setter(self):
        return self._topic_setter

    def topic_timestamp(self):
        return self._topic_timestamp

    def modes(self):
        return self._modes

    def member_data(self, user):
        return ChannelMember(self._users[user])

    def has_member(self, user):
        return self._users.has_key(user)

    def members(self):
        return self._users.keys()

    def join(self, user):
        self.logger().debug("User %s is joining.", repr(user))

        if self.has_member(user):
            self.logger().debug("User already on the channel: %s.", repr(user))
            raise parasha.errors.AlreadyOnChannelError(self.id(), user)

        old_members = self.members()

        self._users[user] = ChannelMemberData()
        self._server._users[user].channels.add(self.id())

        user.do_join(self.id())

        for member in old_members:
            member.join(user, self.id())

    def part(self, user, reason):
        self.logger().debug("User %s is leaving: %s.", repr(user), repr(reason))

        if not self.has_member(user):
            self.logger().debug("User is not on the channel: %s.", repr(user))
            raise parasha.errors.NotOnChannelError(self.id(), user)

        self.remove_user(user)

        user.do_part(self.id(), reason)

        for member in self.members():
            member.part(user, self.id(), reason)

    def kick(self, who, user, reason):
        self.logger().debug("User %s is going to be kicked by %s: %s.",
                            repr(user),
                            repr(who),
                            repr(reason))

        if not self.has_member(user):
            self.logger().debug("User is not on the channel: %s.", repr(user))
            raise parasha.errors.NotOnChannelError(self.id(), user)

        self.remove_user(user)

        user.do_kick(who, self.id(), reason)

        for member in self.members():
            member.kick(who, self.id(), user, reason)

    def remove_user(self, user):
        self.logger().debug("Removing user from the channel: %s.", repr(user))

        if self.has_member(user):
            del self._users[user]

        self._server._users[user].channels.discard(self.id())

    def privmsg(self, source, text):
        self.logger().debug("Privmsg from %s to %s: %s.",
                            repr(source),
                            repr(self.members()),
                            repr(text))

        for member in self.members():
            if member is not source:
                member.privmsg(source, self.id(), text)

    def notice(self, source, text):
        self.logger().debug("Notice from %s to %s: %s.",
                            repr(source),
                            repr(self.members()),
                            repr(text))

        for member in self.members():
            if member is not source:
                member.notice(source, self.id(), text)

    def add_role(self, who, user, role):
        self.logger().debug("%s adds role to user %s: %s.", repr(who), repr(user), repr(role))

        if not self.has_member(user):
            self.logger().debug("User is not on the channel: %s.", repr(user))
            raise parasha.errors.NotOnChannelError(self.id(), user)

        if role.flags() & self._users[user].modes != role.flags():
            added = role.flags() - self._users[user].modes
            self._users[user].modes |= role.flags()

            for member in self.members():
                member.role_change(who, self.id(), "+", "".join(added), user)

    def revoke_role(self, who, user, role):
        self.logger().debug("%s revokes role from user %s: %s.", repr(who), repr(user), repr(role))

        if not self.has_member(user):
            self.logger().debug("User is not on the channel: %s.", repr(user))
            raise parasha.errors.NotOnChannelError(self.id(), user)

        if role.flags() & self._users[user].modes == role.flags():
            self._users[user].modes -= role.flags()

            for member in self.members():
                member.role_change(who, self.id(), "-", "".join(role.flags()), user)

    def set_topic(self, who, text, timestamp = None):
        self.logger().debug("%s sets topic (timestamp: %s): %s.",
                            repr(who),
                            repr(timestamp),
                            repr(text))

        assert isinstance(text, unicode)

        self._topic = text
        self._topic_setter = who.full_name()

        if timestamp is None:
            self._topic_timestamp = int(time.time())
        else:
            self._topic_timestamp = timestamp

        for member in self.members():
            member.topic(who, self.id(), text)
