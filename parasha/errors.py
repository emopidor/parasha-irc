#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class IrcError(Exception):
    pass

class AlreadyOnChannelError(IrcError):
    def __init__(self, channel, user):
        self.channel = channel
        self.user = user

class NotOnChannelError(IrcError):
    def __init__(self, channel, user):
        self.channel = channel
        self.user = user

class CantSendToChannelError(IrcError):
    def __init__(self, channel, source):
        self.channel = channel
        self.source = source

class NoSuchChannelError(IrcError):
    def __init__(self, channel, user):
        self.channel = channel
        self.user = user

class NoSuchNickError(IrcError):
    def __init__(self, nick, source):
        self.nick = nick
        self.source = source

class NickInUseError(IrcError):
    def __init__(self, nick, user):
        self.nick = nick
        self.user = user

class InvalidNickError(IrcError):
    def __init__(self, nick, user):
        self.nick = nick
        self.user = user

class InvalidChannelError(IrcError):
    def __init__(self, channel):
        self.channel = channel

class UsersDontMatchError(IrcError):
    def __init__(self, nick, user):
        self.nick = nick
        self.user = user
