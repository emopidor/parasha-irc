#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

import logging

from parasha.channel_id import ChannelId, normalize_to_channel
from parasha.nick import Nick, normalize_to_nick
from parasha.basic_channel import BasicIrcChannel, ChannelRole, channel_role_prefixes
from parasha.basic_user import BasicIrcUser
import parasha.errors
import parasha.config


__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class IrcUserData(object):
    def __init__(self):
        self.nick = None
        self.ident = None
        self.host = None
        self.real_name = None
        self.modes = set()
        self.channels = set()


class IrcUserDataConstView(object):
    def __init__(self, data):
        self._data = data

    def nick(self):
        return self._data.nick

    def ident(self):
        return self._data.ident

    def host(self):
        return self._data.host

    def real_name(self):
        return self._data.real_name

    def modes(self):
        return self._data.modes

    def channels(self):
        return self._data.channels


class BasicIrcServer(object):
    def __init__(self, name, logger = None):
        assert isinstance(name, unicode)

        self._name = name
        self._logger = logger or logging.getLogger("BasicIrcServer")

        self._channels = dict()
        self._users = dict()
        self._nicks = dict()

    def __repr__(self):
        return "BasicIrcServer(%s)" % repr(self.full_name())

    # Server configuration.
    # IrcActor interface implementation.
    def full_name(self):
        return self._name

    def version(self):
        return parasha.config.version

    def created(self):
        return u"February 14 2008 at 13:37:00 UTC"

    def server_info(self):
        return u"-"

    def supported_umodes(self):
        return set("iwaso")

    def supported_cmodes(self):
        return set("oOvamnqpsrtklbeI")

    def max_nick_length(self):
        return 5 * 1024

    def max_ident_length(self):
        return 5 * 1024

    def max_host_length(self):
        return 5 * 1024

    def max_channel_length(self):
        return 5 * 1024

    def network_name(self):
        return u"ParashaIrc"

    def case_mapping(self):
        return u"ascii"

    def isupport(self):
        role_modes = ""
        role_prefixes = ""

        for role in ChannelRole:
            if len(role.flags()) > 0:
                role_modes += list(role.flags())[0]
                role_prefixes += role.nick_prefix()

        return {
            u"PREFIX": "(%s)%s" % (role_modes, role_prefixes),
            u"NICKLEN": str(self.max_nick_length()),
            u"CHANNELLEN": str(self.max_channel_length()),
            u"NETWORK": self.network_name(),
            u"CASEMAPPING": self.case_mapping()
        }

    # May return None.
    def motd(self):
        return [u"Welcome to the ParashaIrc server!"]

    def hopcount(self, another_server):
        if another_server is self:
            return 0
        else:
            return 1

    def logger(self):
        return self._logger

    # Server data selectors.
    def user_data(self, user):
        return IrcUserDataConstView(self._users[user])

    def has_user(self, user):
        if isinstance(user, Nick):
            return self.has_nick(user)
        else:
            return self._users.has_key(user)

    def users(self):
        return self._users.keys()

    def user(self, who):
        assert isinstance(who, Nick) or isinstance(who, BasicIrcUser)

        if isinstance(who, Nick):
            return self._nicks[who]
        else:
            def do_not_use(*args, **kvargs):
                pass

            do_not_use(self._users[who])

            return who

    def has_nick(self, nick):
        assert isinstance(nick, Nick)

        return self._nicks.has_key(nick)

    def nicks(self):
        return self._nicks.keys()

    def check_nick(self, nick):
        assert isinstance(nick, Nick)

        return (len(nick.original()) != 0 and
                len(nick.original()) <= self.max_nick_length() and
                nick.original()[0] not in ({":"} | channel_role_prefixes()) and
                nick.original().find("!") == -1 and
                nick.original().find(" ") == -1 and
                nick.original().find("\r") == -1 and
                nick.original().find("\n") == -1)

    def _set_nick(self, nick, user):
        self.logger().debug("Setting nick %s to user %s.", repr(nick), repr(user))

        assert isinstance(nick, Nick)

        self._users[user].nick = nick
        self._nicks[nick] = user

    def _release_nick(self, user):
        self.logger().debug("Releasing nick of user %s.", repr(user))

        if user.nick() is not None:
            assert self.has_nick(user.nick())
            assert self.user(user.nick()) is user

            del self._nicks[user.nick()]

    def channel(self, name):
        assert isinstance(name, ChannelId)

        return self._channels[name]

    def has_channel(self, name):
        assert isinstance(name, ChannelId)

        return self._channels.has_key(name)

    def channels(self):
        return self._channels.keys()

    def check_channel_name(self, id):
        assert isinstance(id, ChannelId)

        return (len(id.name()) != 0 and
                len(id.name()) <= self.max_channel_length() and
                id.name()[0] != ":" and
                id.name().find(" ") == -1 and
                id.name().find("\r") == -1 and
                id.name().find("\n") == -1)

    def create_channel(self, name, channel):
        self.logger().debug("Calling create_channel: %s.", repr(name))

        assert isinstance(name, ChannelId)
        assert isinstance(channel, BasicIrcChannel)

        if self.check_channel_name(name):
            self.logger().info("Creating a channel: %s.", repr(name))

            self._channels[name] = channel
        else:
            self.logger().debug("Invalid channel name: %s.", repr(name))

            raise parasha.errors.InvalidChannelError(name)

    def delete_channel(self, name):
        self.logger().debug("Calling delete_channel: %s.", repr(name))

        assert isinstance(name, ChannelId)
        assert len(self._channels[name].members()) == 0

        self.logger().info("Deleting channel %s.", repr(name))

        del self._channels[name]

    def watching_users(self, user):
        user_channels = self.user_data(user).channels()
        visible_users = set(sum([self.channel(channel).members() for channel in user_channels], []))

        return visible_users - {user}

    def add_user(self, user, host):
        self.logger().debug("Calling add_user: %s, %s.", repr(user), repr(host))

        assert isinstance(user, BasicIrcUser)
        assert isinstance(host, unicode)

        self.logger().info("Adding user %s with host %s.", repr(user), repr(host))

        self._users[user] = IrcUserData()
        self._users[user].host = host

    def quit_user(self, user, reason = None):
        self.logger().info("Quiting user %s (%s): %s",
                           repr(user),
                           repr(user.full_name()),
                           repr(reason))

        assert self.has_user(user)

        for watcher in self.watching_users(user):
            watcher.quit(user, reason)

        for channel in set(self.user_data(user).channels()):
            self.channel(channel).remove_user(user)

        self._release_nick(user)

        del self._users[user]

        user.terminate(reason)

    def release_user(self, user, reason = None):
        self.logger().info("Releasing user %s (%s): %s",
                           repr(user),
                           repr(user.full_name()),
                           repr(reason))

        assert self.has_user(user)

        for watcher in self.watching_users(user):
            watcher.quit(user, reason)

        for channel in set(self.user_data(user).channels()):
            self.channel(channel).remove_user(user)

        self._release_nick(user)

        info = self._users[user]

        del self._users[user]

        return info

    def change_nick(self, user, nick):
        self.logger().debug("Call change_nick: %s, %s.", repr(user), repr(nick))

        if self.has_nick(nick):
            self.logger().debug("Nick is already in use: %s.", repr(nick))

            raise parasha.errors.NickInUseError(nick, user)
        elif not self.check_nick(nick):
            self.logger().debug("Invalid nick: %s.", repr(nick))

            raise parasha.errors.InvalidNickError(nick, user)
        else:
            self.logger().info("Changing nick of user %s from %s to %s.",
                               repr(user),
                               repr(user.nick()),
                               repr(nick))

            user.set_nick(nick)

            for watching in self.watching_users(user):
                watching.nick_change(user, nick)

            self._release_nick(user)
            self._set_nick(nick, user)

    def check_ident(self, ident):
        assert isinstance(ident, unicode)

        return (len(ident) != 0 and
                len(ident) <= self.max_ident_length() and
                ident[0] != ":" and
                ident.find("@") == -1 and
                ident.find(" ") == -1 and
                ident.find("\r") == -1 and
                ident.find("\n") == -1)

    def change_ident(self, user, ident):
        self.logger().debug("Calling change_ident: %s, %s.", repr(user), repr(ident))

        assert self.check_ident(ident)

        self.logger().debug("Changing ident of user %s from %s to %s.",
                            repr(user),
                            repr(user.ident()),
                            repr(ident))

        self._users[user].ident = ident

    def change_real_name(self, user, real_name):
        self.logger().debug("Calling change_real_name: %s, %s.", repr(user), repr(real_name))

        assert isinstance(real_name, unicode)

        self.logger().debug("Changing real name of user %s from %s to %s.",
                            repr(user),
                            repr(user.real_name()),
                            repr(real_name))

        self._users[user].real_name = real_name

    def check_host(self, host):
        assert isinstance(host, unicode)

        return (len(host) != 0 and
                len(host) <= self.max_host_length() and
                host[0] != ":" and
                host.find(" ") == -1 and
                host.find("\r") == -1 and
                host.find("\n") == -1)

    def change_host(self, user, host):
        self.logger().debug("Calling change_host: %s, %s.", repr(user), repr(host))

        assert self.check_host(host)

        self.logger().debug("Changing host of user %s from %s to %s.",
                            repr(user),
                            repr(user.host()),
                            repr(host))

        self._users[user].host = host

    def change_user_mode(self, who, user, add, remove):
        self.logger().debug("Changing mode of user %s: %s, +%s, -%s.",
                            repr(user),
                            repr(who),
                            repr(add),
                            repr(remove))

        to_add = add - self._users[user].modes
        to_remove = self._users[user].modes & remove

        if len(to_add) > 0 or len(to_remove) > 0:
            user.user_mode_change(who, user, to_add, to_remove)

        self._users[user].modes = (self._users[user].modes | to_add) - to_remove

        self.logger().debug("New modes of user %s: %s.",
                            repr(user),
                            repr(self._users[user].modes))

    def join(self, user, channel):
        self.logger().debug("Calling join: %s, %s.", repr(user), repr(channel))
        if not self.has_channel(channel):
            self.logger().debug("No such channel: %s.", repr(channel))
            raise parasha.errors.NoSuchChannelError(channel, user)
        else:
            self.channel(channel).join(user)

    def part(self, user, channel, reason):
        self.logger().debug("Calling part: %s, %s, %s.", repr(user), repr(channel), repr(reason))

        if not self.has_channel(channel):
            self.logger().debug("No such channel: %s.", repr(channel))

            raise parasha.errors.NoSuchChannelError(channel, user)
        else:
            self.channel(channel).part(user, reason)

    def kick(self, who, channel, user, reason = None):
        self.logger().debug("Calling kick: %s, %s, %s, %s.",
                            repr(who),
                            repr(channel),
                            repr(user),
                            repr(reason))

        if not self.has_channel(channel):
            self.logger().debug("No such channel: %s.", repr(channel))

            raise parasha.errors.NoSuchChannelError(channel, who)
        elif not self.has_user(user):
            self.logger().debug("No such user: %s.", repr(user))

            raise parasha.errors.NoSuchNickError(user, who)
        else:
            self.channel(channel).kick(who, self.user(user), reason)

    def privmsg(self, source, target, text):
        self.logger().debug("Calling privmsg: %s, %s, %s.", repr(source), repr(target), repr(text))

        maybe_nick = normalize_to_nick(target)
        maybe_channel = normalize_to_channel(target)

        if self.has_user(maybe_nick or target):
            user = self.user(maybe_nick or target)

            self.logger().debug("Sending privmsg from %s to user %s: %s.",
                                repr(source),
                                repr(user),
                                repr(text))

            user.privmsg(source, user, text)
        elif maybe_channel is not None and self.has_channel(maybe_channel):
            self.channel(maybe_channel).privmsg(source, text)
        else:
            self.logger().debug("No such user: %s.", repr(target))

            raise parasha.errors.NoSuchNickError(target, source)

    def notice(self, source, target, text):
        self.logger().debug("Calling notice: %s, %s, %s.", repr(source), repr(target), repr(text))

        maybe_nick = normalize_to_nick(target)
        maybe_channel = normalize_to_channel(target)

        if self.has_user(maybe_nick or target):
            user = self.user(maybe_nick or target)

            self.logger().debug("Sending notice from %s to user %s: %s.",
                                repr(source),
                                repr(user),
                                repr(text))

            user.notice(source, user, text)
        elif maybe_channel is not None and self.has_channel(maybe_channel):
            self.channel(maybe_channel).notice(source, text)
        else:
            self.logger().debug("No such user: %s.", repr(target))

            raise parasha.errors.NoSuchNickError(target, source)
