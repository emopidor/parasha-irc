#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

import re

import enum

import tornado.iostream


__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class IrcMessage(object):
    # https://tools.ietf.org/html/rfc2812#section-2.3.1
    _message_pattern = re.compile(
        r"^(:(?P<prefix>[^ ]+) )?" +
        r"(?P<command>[A-Za-z]+|\d{3})" +
        "(((?P<params1>( +[^\0\r\n :][^\0\r\n ]*){0,13})( +:(?P<trailing1>[^\0\r\n]*))?)|" +
        "((?P<params2>( +[^\0\r\n :][^\0\r\n ]*){14})( +:?(?P<trailing2>[^\0\r\n]*))?)) *\r?\n$"
    )

    def __init__(self, data = None):
        self.prefix = None
        self.command = None
        self.args = None

        if data:
            self.parse(data)

    def parse(self, data):
        try:
            parsed = IrcMessage._message_pattern.match(data.decode("utf-8"))
        except UnicodeDecodeError:
            return

        if parsed:
            prefix = parsed.group("prefix")
            command = parsed.group("command").upper()
            args = []

            params = parsed.group("params1") or parsed.group("params2")

            trailing = (parsed.group("trailing1")
                        if parsed.group("trailing1") is not None
                        else parsed.group("trailing2"))

            if params is not None and len(params) > 0:
                args = [arg for arg in params.lstrip(" ").split(" ")]

            if trailing is not None:
                args.append(trailing)

            if prefix is not None:
                self.prefix = prefix

            self.command = command
            self.args = args


    def valid(self):
        return self.command is not None


class IrcConnectionState(enum.Enum):
    connected = 1
    disconnected = 2


class BasicIrcConnection(object):
    def __init__(self, stream, logger, max_message_length = 513 * 1024):
        self.__stream = stream

        self.__logger = logger
        self.__send_logger = logger.getChild("send")
        self.__recv_logger = logger.getChild("recv")

        self.__max_message_length = max_message_length

        self.__state = IrcConnectionState.connected

        self.__handlers = dict()

        self.__stream.set_close_callback(self._on_close)

    def logger(self):
        return self.__logger

    def max_message_length(self):
        return self.__max_message_length

    def state(self):
        return self.__state

    def send_message(self, prefix, command, args, without_colon = False):
        if self.state() == IrcConnectionState.disconnected:
            return

        self.__send_logger.debug("Sending %s, %s, %s.", repr(prefix), repr(command), repr(args))

        def to_string(arg):
            if arg is None:
                return ""
            elif isinstance(arg, str):
                # Just check that it's utf-8.
                arg.decode("utf-8")
                return arg
            elif isinstance(arg, unicode):
                return arg.encode("utf-8")
            else:
                return str(arg)

        def check_last_argument(arg):
            return (arg.find("\r") == -1 and
                    arg.find("\n") == -1 and
                    arg.find("\0") == -1)

        def check_argument(arg):
            return check_last_argument(arg) and arg.find(" ") == -1 and not arg.startswith(":")

        # Because RFC
        assert len(args) <= 15

        prefix_string = "" if prefix is None else (":" + to_string(prefix) + " ")

        args_string = ""

        if len(args) > 0:
            args = [to_string(arg) for arg in args]

            for arg in args[:-1]:
                assert check_argument(arg)
                args_string += " " + arg

            last_arg = args[-1]

            assert check_last_argument(last_arg)

            if (without_colon and
                len(last_arg) > 0 and
                last_arg.find(" ") == -1 and
                not last_arg.startswith(":")):

                args_string += " " + last_arg
            else:
                args_string += " :" + last_arg

        data = prefix_string + to_string(command) + args_string + "\r\n"

        self.__send_logger.debug("The blob: %s.", repr(data))

        try:
            self.__stream.write(data)
        except tornado.iostream.StreamBufferFullError:
            self.__send_logger.warning("The stream buffer is full. Unable to send messages, so dropping the client.")

            self.drop("The IO buffer overflow.")
        except tornado.iostream.StreamClosedError:
            pass

    def set_handler(self, message_type, handler):
        self.__handlers[message_type.upper()] = handler

    def process_message(self, message):
        if self.__handlers.has_key(message.command):
            self.__handlers[message.command](message)
        else:
            self.on_unknown_message(message)

    def on_unknown_message(self, message):
        pass

    def run(self):
        self.__read_next_message()

    def close(self):
        self.__stream.close()
        self.__state = IrcConnectionState.disconnected

    def drop(self, reason):
        if self.state() != IrcConnectionState.disconnected:
            self.logger().debug("Dropping the connection: %s.", repr(reason))

            self.close()

    def _on_close(self):
        self.logger().debug("The connection is closed.")

        self.__state = IrcConnectionState.disconnected

    def __read_next_message(self):
        try:
            self.__stream.read_until("\n",
                                     self.__on_message,
                                     max_bytes = self.max_message_length())
        except tornado.iostream.StreamClosedError:
            pass
        except tornado.iostream.UnsatisfiableReadError:
            pass

    def __on_message(self, data):
        if self.state() == IrcConnectionState.disconnected:
            return

        self.__recv_logger.debug("Message received: %s.", repr(data))

        message = IrcMessage(data)

        if message.valid():
            self.__recv_logger.debug("Parsed: %s, %s, %s.",
                                     repr(message.prefix),
                                     repr(message.command),
                                     repr(message.args))

            self.process_message(message)

            self.__read_next_message()
        else:
            self.__recv_logger.info("Unable to parse message from the client. Terminating the connection.")

            self.drop("Garbage in the socket.")
