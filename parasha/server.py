#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

from parasha.basic_server import BasicIrcServer
from parasha.basic_channel import BasicIrcChannel
import parasha.errors


__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class AutomaticIrcChannel(BasicIrcChannel):
    def __init__(self, *args, **kvargs):
        BasicIrcChannel.__init__(self, *args, **kvargs)

    def remove_user(self, user):
        BasicIrcChannel.remove_user(self, user)

        if len(self.members()) == 0:
            self.server().delete_channel(self.id())

    def privmsg(self, source, text):
        if self.has_member(source) or source.is_ircop():
            BasicIrcChannel.privmsg(self, source, text)
        else:
            self.logger().debug("%s can't privmsg to the channel: %s.", repr(source), repr(text))

            raise parasha.errors.CantSendToChannelError(self.id(), source)

    def notice(self, source, text):
        if self.has_member(source) or source.is_ircop():
            BasicIrcChannel.notice(self, source, text)
        else:
            self.logger().debug("%s can't notice to the channel: %s.", repr(source), repr(text))

            raise parasha.errors.CantSendToChannelError(self.id(), source)


class IrcServer(BasicIrcServer):
    def __init__(self, *args, **kvargs):
        BasicIrcServer.__init__(self, *args, **kvargs)

    def channel_prefixes(self):
        return {"#", "&"}

    def isupport(self):
        result = BasicIrcServer.isupport(self)
        result["CHANTYPES"] = "".join(self.channel_prefixes())

        return result

    def check_nick(self, nick):
        return (BasicIrcServer.check_nick(self, nick) and
                nick.original()[0] not in self.channel_prefixes())

    def check_channel_name(self, id):
        return (BasicIrcServer.check_channel_name(self, id) and
                id.name()[0] in self.channel_prefixes())

    def join(self, user, channel):
        if not self.has_channel(channel):
            self.create_channel(channel, AutomaticIrcChannel(self, channel))
            just_created = True
        else:
            just_created = False

        try:
            BasicIrcServer.join(self, user, channel)
        except:
            if just_created:
                self.delete_channel(channel)

            raise
