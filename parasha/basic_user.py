#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2015, Kirisaki Chitoge
#
# This file is part of Parasha.
#
# Parasha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Parasha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Parasha.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Kirisaki Chitoge"
__copyright__ = "Copyright 2015, Kirisaki Chitoge"
__credits__ = ["Kirisaki Chitoge"]
__license__ = "GNU LGPL version 3"


class BasicIrcUser(object):
    def __init__(self, server):
        self._server = server

    # IrcUser interface.
    def terminate(self, reason = None):
        pass

    def privmsg(self, source, target, text):
        pass

    def notice(self, source, target, text):
        pass

    def user_mode_change(self, who, user, add, remove):
        pass

    def role_change(self, who, channel, action, flag, user):
        pass

    def set_nick(self, nick):
        pass

    def nick_change(self, who, new_nick):
        pass

    def do_join(self, channel):
        pass

    def join(self, user, channel):
        pass

    def do_part(self, channel, reason):
        pass

    def part(self, user, channel, reason):
        pass

    def quit(self, user, reason):
        pass

    def topic(self, who, channel, text):
        pass

    def do_kick(self, who, channel, reason):
        pass

    def kick(self, who, channel, user, reason):
        pass

    # Getters. Should not be overriden.
    def nick(self):
        return self.server().user_data(self).nick()

    def ident(self):
        return self.server().user_data(self).ident()

    def host(self):
        return self.server().user_data(self).host()

    def real_name(self):
        return self.server().user_data(self).real_name()

    def modes(self):
        return self.server().user_data(self).modes()

    def is_away(self):
        return "a" in self.modes()

    def is_ircop(self):
        return "o" in self.modes()

    def server(self):
        return self._server

    # IrcActor interface implementation.
    def full_name(self):
        try:
            return self.nick().original() + u"!" + self.ident() + u"@" + self.host()
        except:
            return None

    # Target interface implementation.
    def short_name(self):
        return self.nick().original()
